const firebaseConfig = {
    apiKey: "AIzaSyCCSSVdCuBnP9wajaRHmKkdGo2kAYJHvoc",
  authDomain: "administrador-web-60756.firebaseapp.com",
  databaseURL: "https://administrador-web-60756-default-rtdb.firebaseio.com",
  projectId: "administrador-web-60756",
  storageBucket: "administrador-web-60756.appspot.com",
  messagingSenderId: "989786683525",
  appId: "1:989786683525:web:c15065e13166e74daa11ec"
};


// Inicializar Firebase
firebase.initializeApp(firebaseConfig);

// Referencia a la ubicación en la base de datos donde se guardarán los productos
const productosRef = firebase.database().ref('productos');

// Obtener referencia al formulario
const productForm = document.getElementById('productForm');

// Manejar el envío del formulario
productForm.addEventListener('submit', (e) => {
    e.preventDefault();

    // Obtener los valores del formulario
    const codigo = document.getElementById('codigo').value;
    const nombre = document.getElementById('nombre').value;
    const precio = document.getElementById('precio').value;
    const status = document.getElementById('status').value;
    const url = document.getElementById('url').value;

    // Verificar que los campos no estén vacíos
    if (codigo && nombre && precio && status && url) {
        // Crear un objeto con los datos del producto
        const producto = {
            codigo: codigo,
            nombre: nombre,
            precio: precio,
            status: status,
            url: url
        };

        // Agregar el producto a la base de datos
        productosRef.push(producto)
            .then(() => {
                alert('Producto agregado correctamente.');
                // Limpiar el formulario después de agregar el producto
                productForm.reset();
            })
            .catch((error) => {
                alert('Error al agregar el producto: ' + error.message);
            });
    } else {
        alert('Por favor, completa todos los campos.');
    }
});


