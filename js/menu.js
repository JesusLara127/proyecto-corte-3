const sidebar = document.querySelector('.sidebar');

        function openSidebar() {
            document.querySelector('.sidebar').classList.add('open');
            // Guarda el estado de la barra lateral en localStorage
            localStorage.setItem('sidebarStatus', 'open');
        }
        
        // Función para cerrar la barra lateral
        function closeSidebar() {
            document.querySelector('.sidebar').classList.remove('open');
            // Guarda el estado de la barra lateral en localStorage
            localStorage.setItem('sidebarStatus', 'closed');
        }
        
        // Verifica el estado de la barra lateral al cargar la página
        document.addEventListener('DOMContentLoaded', function() {
            const sidebarStatus = localStorage.getItem('sidebarStatus');
            if (sidebarStatus === 'open') {
                openSidebar();
            } else {
                closeSidebar();
            }
        });
        
        const openButton = document.getElementById('menu-toggle-open');
        const closeButton = document.getElementById('menu-toggle-close');
        
        openButton.addEventListener('click', openSidebar);
        closeButton.addEventListener('click', closeSidebar);